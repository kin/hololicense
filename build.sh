

export TZ=UTC
tic=$(date +%s)
date=$(date +%D)
perl -S moustache.pl LICENSE.md LICENSE.md~
pandoc -f markdown -t latex -o LICENSE.pdf LICENSE.md~
pandoc -f markdown -t plain -o LICENSE.txt LICENSE.md~


perl -S moustache.pl _layouts/default.html _layouts/default.htm~
pandoc -f markdown -t html --template _layouts/default.htm~ -o index.html LICENSE.md~
#pandoc -f markdown -t latex --template _layouts/default.htm~ -o index.pdf LICENSE.md~
rm LICENSE.md~
qm=$(ipfs add -Q -w LICENSE.* qm.log)
echo $tic: $qm >> qm.log
qm=$(ipfs add -Q -w LICENSE.* qm.log)
echo url: https://gateway.ipfs.io/ipfs/$qm
sed -i -e "s/qm: .*/qm: $qm/" _data/ipfs.yml
sed -i -e "s/qm: .*/qm: $qm/" -e "s/date: .*/date: $(date)/" holoWork/README.md
git add LICENSE.pdf LICENSE.txt qm.log _data/ipfs.yml holoWork
git commit -a -m "as per $date"
git push


