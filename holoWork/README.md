---
layout: default
qm: QmdUrr24boZMecWBFyjs3QTxPsv17bCMTaoHnwnYPx1GQo
date: Sun May 24 10:54:12 UTC 2020
parentid: c1bd9d687b271656343825b3aa351b16260f4d51
---
<!-- {% assign qm = site.data.ipfs.qm %} -->
<!-- {% assign sqm = qm | slice: 2,7 %} -->
<!-- {% assign aaid = site.data.source.aaid %}
## The Work #{{sqm}} ({{page.date}})

This page purpose is set a [*record*date][date] for the creation of the work #{{sqm}}

The holoAtom ({{sqm}}) is filed on [IPFS] on {{page.date}} as **{{page.qm}}**

* [/ipfs/{{qm}}](http://ipfs.blockring™.ml/ipfs/{{qm}})
* [#{{sqm}}](https://qwant.com/?q=%26+%23{{sqm}})
* artifact: https://sys.artifacts.ai/6ms3h/
* git's commitid: [{{aaid}}]

The proof of existance can be found on [artifacts.ai][aai] (or github: [{{page.aaid}}][gh])


note this work is distributed under the HoloKIN License: [HKL License](https://krysthal-intelligence-network.github.io/holoLicense/)


[IPFS]: https://ipfs.io
[qm.md]: https://sys.artifacts.ai/92j6c/?ref={{page.aaid}}
[aai]: https://sys.artifacts.ai/6ms3h/
[holofacts]: https://sys.artifacts.ai/y4ewv/
[date]: https://duckduckgo.com/?q=!g+what+happened+on+{{page.date}}
[gh]: https://github.com/search?q={{aaid}}&type=Commits
[{{aaid}}]: https://duckduckgo.com/?q=!gh+{{aaid}}&type=Commits
