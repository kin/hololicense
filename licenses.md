# HoloKin Licences

* our plateform (the HoloWork)is distributed under the HoloKIN License :
   [HKLicense](LICENSE.txt)
   
```txt
provided that the
above copyright notice appear in all copies and that both that copyright notice
and this permission notice appear in supporting documentation, and that the
name of <copyright holder> not be used in advertising or publicity pertaining
to distribution of the software without specific, written prior permission.
<copyright holder> makes no representations about the suitability of this
software for any purpose.  It is provided "as is" without express or implied
warranty.
```

other license we support

* [COIL License](http://copyfree.org/content/standard/licenses/coil/license.txt)
* [A License](http://copyfree.org/content/standard/licenses/amanda/license.txt)
* [GAL License](http://copyfree.org/content/standard/licenses/gal/license.txt)
* [CC0 License](http://copyfree.org/content/standard/licenses/cc0/license.txt)
* [MIT License](http://copyfree.org/content/standard/licenses/mit/license.txt)
* [GNU License](http://copyfree.org/content/standard/licenses/gnu_all_permissive/license.txt)
* [GIYOL License](http://copyfree.org/content/standard/licenses/giyoll/license.txt)
* [Tiny License](http://copyfree.org/content/standard/licenses/tiny/license.txt)
* [WTFPL License](http://copyfree.org/content/standard/licenses/wtfpl2/license.txt)
* [Un License](http://copyfree.org/content/standard/licenses/un/license.txt)

* [CopyFree Licenses](http://copyfree.org/standard/licenses)




--&nbsp;<br>
![F](img/copyfree.png)
![F](img/hololic.svg)
